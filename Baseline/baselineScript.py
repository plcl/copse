#!/usr/bin/env python3
# -*- coding: utf-8 -*-

######################################################
#
# Author:      Vidush Singhal    
# Institution: Purdue University
# Advisor:     Professor Milind Kulkarni
# created:     Feb 18th 2021
#
#######################################################

import os
import subprocess
import re
import numpy as np
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from statistics import median
import pandas as pd
import sys

#################################################################Change the settings according to the instructions#######################################################

#rootdir = '/root'

if len(sys.argv) < 5:
    sys.exit("Incorrect Usage: python3 baselineScript.py parallel/serial benchmark-name threads iterations")  

#Select the root directory, i.e, the path to the folder in which this file exists.
rootdir = os.getcwd()
#rootdir = input("Enter the path to the current Directory: ")
#rootdir = str(rootdir)

#set the library and include paths for IntelTBB, for instance here library path is set by assigning variable IntelTBB_lib the string '-L ~/HElib/build/helib_pack/lib' 
#where ~/HElib/build/helib_pack/lib is the path to the Helib lib directory in either the build folder or the install folder
#
#For installation of Intel TBB, the below steps can be followed
#
#   git clone https://github.com/wjakob/tbb.git
#   cd tbb/build
#   cmake -DCMAKE_INSTALL_PREFIX=~/IntelTBB ..
#   make -j 10
#   sudo make install

#intelTBB_lib = input("Enter the full library path to intel tbb: ")
#intelTBB_inc = input("Enter the full include path to intel tbb: ")

intelTBB_lib = "~/.local/lib"
intelTBB_inc = "~/.local/include"

IntelTBB_lib = "-L " + str(intelTBB_lib) 
IntelTBB_inc = "-I " + str(intelTBB_inc)

#IntelTBB_lib = '-L /root/HElib/build/helib_pack/lib'
#IntelTBB_inc = '-L /root/IntelTBB/lib/'

#set the library and include paths for Helib
#
# for installation of Helib see the link https://github.com/homenc/HElib
#
# the Helib version was build from commit 5c90a506a4a4ef3814d1ccc0e7403ac493638d11
# Link : https://github.com/homenc/HElib/commit/5c90a506a4a4ef3814d1ccc0e7403ac493638d11
#

#helib_lib = input("Enter the full library path to intel HElib: ")
#helib_inc = input("Enter the full include path to intel HElib: ")

helib_lib = intelTBB_lib
helib_inc = intelTBB_inc

Helib_lib = "-L " + str(helib_lib) 
Helib_inc = "-I " + str(helib_inc)

#Helib_lib = '-I /root/HElib/build/helib_pack/include/'
#Helib_inc = '-I /root/IntelTBB/include/'


SUBDIR = "Baseline/benchmarks"

#####################################################################End of user settings##################################################################################

SERIAL_RUN = {}
PARALLEL_RUN = {}

SERIAL_RUN['depth4'] = []
SERIAL_RUN['depth5'] = []
SERIAL_RUN['depth6'] = []
SERIAL_RUN['income_5'] = []
SERIAL_RUN['soccer_5'] = []
SERIAL_RUN['income_15'] = []
SERIAL_RUN['soccer_15'] = []
SERIAL_RUN['prec8'] = []
SERIAL_RUN['prec16'] = []
SERIAL_RUN['width55'] = []
SERIAL_RUN['width78'] = []
SERIAL_RUN['width677'] = []

PARALLEL_RUN['depth4'] = []
PARALLEL_RUN['depth5'] = []
PARALLEL_RUN['depth6'] = []
PARALLEL_RUN['income_5'] = []
PARALLEL_RUN['soccer_5'] = []
PARALLEL_RUN['income_15'] = []
PARALLEL_RUN['soccer_15'] = []
PARALLEL_RUN['prec8'] = []
PARALLEL_RUN['prec16'] = []
PARALLEL_RUN['width55'] = []
PARALLEL_RUN['width78'] = []
PARALLEL_RUN['width677'] = []

runs     = ['parallel', 'serial']
MICRO    = ['depth4', 'depth5', 'depth6', 'prec8', 'prec16', 'width55', 'width78', 'width677']
MACRO    = ['income_5', 'soccer_5']
MACRO15  = ['income_15', 'soccer_15']
FILES    = ['depth4', 'depth5', 'depth6', 'income_5', 'soccer_5', 'prec8', 'prec16', 'width55', 'width78', 'width677']
POSSIBLE = ['depth4', 'depth5', 'depth6', 'income_5', 'soccer_5', 'prec8', 'prec16', 'width55', 'width78', 'width677', 'income_15', 'soccer_15']

   
if (sys.argv[1] != "all"):
    if (sys.argv[1] not in runs):
        sys.exit("Incorrect argv 1 should be parallel or serial")  
    runs = [str(sys.argv[1])]

if (sys.argv[2] != "all"):
    if (sys.argv[2] not in POSSIBLE and sys.argv[2] != "1hr" and sys.argv[2] != "micro" and sys.argv[2] != "macro5" and sys.argv[2] != "macro15"):
        sys.exit("Incorrect argv 2 should have correct benchmark names, see README for all benchmark names")     
    FILES = [str(sys.argv[2])]

#set the number of threads/cores that you want to use for the parallel run    
numThreads = int(sys.argv[3])

#set the number of iterations you want to run the experiment for
iterations = int(sys.argv[4])

if (sys.argv[2] == "1hr"):
    FILES = ['depth4', 'width55', 'prec8', 'depth5']
    iterations = 1

if (sys.argv[2] == "micro"):
    FILES = MICRO

if (sys.argv[2] == "macro5"):
    FILES = MACRO

if (sys.argv[2] == "macro15"):
    FILES = MACRO15

src = "../src/*.cpp"    

objectName = 'main'
threads = 0

precision = '8'

for run in runs:
     
    if (run == 'serial'):
        threads = 1
    elif (run == 'parallel'):
        threads = numThreads        
    
    for i in range(0, iterations):
        for subdir, dirs, files in os.walk(rootdir):
            # subdirectories where the c++ compilation occurs have dirs == []
            if (dirs == [] and (str(SUBDIR) in str(subdir))):
                
                #command to used for compiling the file main.cpp file
                cmd = "(" + "cd " + subdir + " && " + "g++ " + "-std=c++14 " + "-O3 " + str(IntelTBB_lib) + " " + str(Helib_lib) + " " +  str(IntelTBB_inc) + " " + str(Helib_inc) + " " + str(src) + " -o " + objectName + " -lntl -lhelib -lpthread -ltbb" + ")"
                
                subprocess.call(cmd, shell=True) 
                
                for fileName in FILES:
                
                    print("subdir: " + str(subdir))
                    print("dirs: " + str(dirs))
                    print("files: " + str(files))
                    
                    #making a file to pipe the commad line argument to a file called main.txt for each subdirectory
                    runtimeFile1 = subdir + "/" + objectName + "1" + ".txt"
                    
                    if "prec16" in str(fileName):
                        precision = '16'
                    else:
                        precision = '8'
                    
                    if "depth4" in str(fileName):
                            
                        print("Executing " + run + " run " + str(i) + " for depth4 bechmark")
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " depth4.tree " + str(threads) + " " + str(precision)  + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0
                        
                        if (run == 'serial'):
                            SERIAL_RUN['depth4'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['depth4'].append(data)    
                            
                    elif "depth5" in str(fileName):                    
                    
                        print("Executing " + run + " run " + str(i) + " for depth5 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " depth5.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['depth5'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['depth5'].append(data)    

                    elif "depth6" in str(fileName):                    
                        
                        print("Executing " + run + " run " + str(i) + " for depth6 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " depth6.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['depth6'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['depth6'].append(data)    

                    
                    elif "income_5" in str(fileName):                    
                    
                        print("Executing " + run + " run " + str(i) + " for income_5 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " income_5.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['income_5'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['income_5'].append(data)
                    
                    elif "income_15" in str(fileName):                    
                    
                        print("Executing " + run + " run " + str(i) + " for income_15 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " income15.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['income_15'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['income_15'].append(data)	   
                            
                    elif "soccer_5" in str(fileName):
                        
                        print("Executing " + run + " run " + str(i) + " for soccer_5 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " soccer_5.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['soccer_5'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['soccer_5'].append(data)
                        
                    elif "soccer_15" in str(fileName):
                        
                        print("Executing " + run + " run " + str(i) + " for soccer_15 bechmark")    
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " soccer15.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['soccer_15'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['soccer_15'].append(data)   
                    
                    elif "prec8" in str(fileName):
                        
                        print("Executing " + run + " run " + str(i) + " for prec8 bechmark")     
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " prec8.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['prec8'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['prec8'].append(data)   
                    
                    elif "prec16" in str(fileName):
                        
                        print("Executing " + run + " run " + str(i) + " for prec16 bechmark")
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " prec16.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['prec16'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['prec16'].append(data)   
                    
                    elif "width55" in str(fileName):

                        print("Executing " + run + " run " + str(i) + " for width55 bechmark")
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " width55.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['width55'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['width55'].append(data)   
                    
                    elif "width78" in str(fileName):
                            
                        print("Executing " + run + " run " + str(i) + " for width78 bechmark")
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " width78.tree " + str(threads) + " " + str(precision) + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['width78'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['width78'].append(data)   
                    
                    elif "width677" in str(fileName):
                            
                        print("Executing " + run + " run " + str(i) + " for width677 bechmark")
                        exe1 = "(" + "cd " + subdir + " && " + "(" + "./" + objectName + " width677.tree " + str(threads) + " " + str(precision)  + " 2> " + objectName + "1" + ".txt" + ")" + ")"                
                        subprocess.call(exe1, shell=True)
                        
                        data = open(runtimeFile1, 'r').read()
                        data = re.findall("\d+", data)
                        if len(data) > 0:
                            data = float(data[0])
                        else:
                            data = 0

                        if (run == 'serial'):
                            SERIAL_RUN['width677'].append(data)
                        elif (run == 'parallel'):
                            PARALLEL_RUN['width677'].append(data)
                            
        
        workbookSerial = Workbook()
        workbookParr = Workbook()

        SERIAL_Sheet = workbookSerial.create_sheet("serial_run", 0)
        PARALLEL_Sheet = workbookParr.create_sheet("parallel_run", 0)

        sheet_to_remove = workbookSerial["Sheet"]
        workbookSerial.remove(sheet_to_remove)
        
        sheet_to_remove = workbookParr["Sheet"]
        workbookParr.remove(sheet_to_remove)

        sheetListSerial = workbookSerial.sheetnames
        sheetListParr =   workbookParr.sheetnames 

        for sheets in sheetListSerial:
    
            if sheets == "serial_run":
        
                i = 1
                for types in SERIAL_RUN:
                    j = 1
                    column_letter = get_column_letter(i)
                    SERIAL_Sheet[column_letter + str(j)] = types
                    i = i + 1
                    j = j + 1
                    for runtimes in SERIAL_RUN[types]:
                        SERIAL_Sheet[column_letter + str(j)] = runtimes
                        j = j + 1
                    j = j + 1
                    SERIAL_Sheet[column_letter + str(j)] = "Mean"
                    j = j + 1
                    if len(SERIAL_RUN[types]) > 0: 
                        SERIAL_Sheet[column_letter + str(j)] = sum(SERIAL_RUN[types]) / len(SERIAL_RUN[types])
                    j = j + 1
                    SERIAL_Sheet[column_letter + str(j)] = "Median"
                    j = j + 1
                    if len(SERIAL_RUN[types]) > 0:
                        SERIAL_Sheet[column_letter + str(j)] = median(SERIAL_RUN[types])
        
        for sheets in sheetListParr:
            
            if sheets == "parallel_run":
        
                i = 1
                for types in PARALLEL_RUN:
                    j = 1
                    column_letter = get_column_letter(i)
                    PARALLEL_Sheet[column_letter + str(j)] = types
                    i = i + 1
                    j = j + 1
                    for runtimes in PARALLEL_RUN[types]:
                        PARALLEL_Sheet[column_letter + str(j)] = runtimes
                        j = j + 1
                    j = j + 1
                    PARALLEL_Sheet[column_letter + str(j)] = "Mean"
                    j = j + 1
                    if len(PARALLEL_RUN[types]) > 0: 
                        PARALLEL_Sheet[column_letter + str(j)] = sum(PARALLEL_RUN[types]) / len(PARALLEL_RUN[types])
                    j = j + 1
                    PARALLEL_Sheet[column_letter + str(j)] = "Median"
                    j = j + 1
                    if len(PARALLEL_RUN[types]) > 0:
                        PARALLEL_Sheet[column_letter + str(j)] = median(PARALLEL_RUN[types])

        #Excel file saved in "Results.xlsx"
        if (str(sys.argv[1]) == "serial" or str(sys.argv[1]) == "all"):
            workbookSerial.save("ResultsSerial.xlsx")
            read_file = pd.read_excel ("ResultsSerial.xlsx")
            read_file.to_csv ("ResultsSerial.csv",  index = None, header=True)
        
        if (str(sys.argv[1]) == "parallel" or str(sys.argv[1]) == "all"):
            workbookParr.save("ResultsParallel.xlsx")
            read_file = pd.read_excel ("ResultsParallel.xlsx")
            read_file.to_csv ("ResultsParallel.csv",  index = None, header=True)


if (str(sys.argv[1]) == "serial" or str(sys.argv[1]) == "all"):
    cmd = "(" + "cd " + rootdir + " && " + "rm" + " ResultsSerial.xlsx" + ")"
    subprocess.call(cmd, shell=True)

if (str(sys.argv[1]) == "parallel" or str(sys.argv[1]) == "all"):
    cmd = "(" + "cd " + rootdir + " && " + "rm" + " ResultsParallel.xlsx" + ")"
    subprocess.call(cmd, shell=True)
