#ifndef ENCODE_INFO_H 
#define ENCODE_INFO_H
#include <iostream>
#include <helib/helib.h>
#include <helib/FHE.h>
#include <vector>

                                                                                               
class EncInfo{                                                                                
                                                                                               
public:                                                                                           
    helib::Context context;                                                                    
    helib::SecKey *sk;                                                                         
    long nslots;
    int bits;
    EncInfo(int m, int p, int r, int bits, int c) : context(m, p, r)                           
    {        
        bits = bits;
        helib::buildModChain(context, bits, c);                                                
        sk = new helib::SecKey(context);                                                       
        sk->GenSecKey();                                                                       
        helib::addSome1DMatrices(*sk);                                                         
        nslots = context.ea->size();                                                           
    }
                                                                                               
};
#endif
                                                                                               
                                                                                               
                                                                                               
                                                                                               
