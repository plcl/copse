#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include <helib/FHE.h>
#include <map>

class DecisionTree{
    
public:
    int ID;
    std::vector<std::string> plaintextFeatures;
    std::vector<long> plaintextThresholds;
    std::vector<long> plaintextLables;
    
public:
    DecisionTree(int ID);
    void Print();
    
};
