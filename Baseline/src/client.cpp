#include "client.h"
#include <iterator>
#include <stdio.h> 
#include <stdlib.h>
#include <time.h>  

Client::Client(EncInfo& info, std::vector<std::string> FeatureMap, int precision){
    
   srand (time(NULL));
   
   int it;
   for (it = 0; it < FeatureMap.size(); it++)
   {
       features.push_back(FeatureMap[it]);
       thresholdVals.push_back(rand());
   }
   
   for(int k=0; k < thresholdVals.size(); k++){
       
       std::vector<long> thresholdBits;
       for (long i = 0; i < precision; i++){
           long temp = (thresholdVals[k] & (1 << i)) >> i;
           thresholdBits.push_back(temp);
       }
       
       const helib::PubKey& public_key = *info.sk;
       const helib::EncryptedArray& ea = *(info.context.ea);

       std::vector<helib::Ctxt> encBits;
       for (int i = 0; i < thresholdBits.size(); i++){
           helib::Ctxt encryptedBit(*info.sk);
            ea.encrypt(encryptedBit, public_key, std::vector<long>(info.context.ea->size(), thresholdBits[i]));
            encBits.push_back(encryptedBit);
        }
        encThresholds.push_back(encBits);
        featureToEncryptionMap[features[k]] = encBits;
    }
    
};


std::vector<std::vector<helib::Ctxt>> Client::getEncryptedThresholds(){
    
    return encThresholds;
};

std::vector<std::string> Client::getFeatures(){
    return features;
};

std::map<std::string, std::vector<helib::Ctxt>> Client::getFeatureMap(){
    
    return featureToEncryptionMap;
}
    
Client::~Client(){};
