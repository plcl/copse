#include "evaluator.hh"
#include <typeinfo> 
#include <tuple>
#include <NTL/BasicThreadPool.h>
#include <chrono>

Evaluator::Evaluator(){
    
}

/************************************************************************************************************
 * Secure Comparison protocol (SecComp): Functions on a single decision tree.                               *
 * Inputs: feature x \epsilon X (where x represents a feature in the client's Encrypted feature vector X)   *
 *         feature y \epsilon Y (where y represents a threshold value belonging to Y)                       *
 * x, y type: x and y are representations of features and thresholds in bitwise form.                       *
 * Output: produces a single bit output (b) for each bit array x and bit array y                            *
 ************************************************************************************************************/

helib::Ctxt Evaluator::SecComp(std::vector<helib::Ctxt> featureVec, std::vector<helib::Ctxt> thresholdVec){
    
    std::vector<helib::Ctxt> LessThan = featureVec;
    std::vector<helib::Ctxt> Equals = featureVec;
    int i;
    int N = thresholdVec.size();
    
    for(i=0; i<N; i++){
        LessThan[i].addConstant(NTL::ZZ(1));
        LessThan[i].multiplyBy(thresholdVec[i]);
        Equals[i] += thresholdVec[i];
        Equals[i].addConstant(NTL::ZZ(1));
    }
    
    //store the intermediate computation of bits b in temp    
    int j;    
    std::vector<helib::Ctxt> CachedVals;
    helib::Ctxt temp = Equals[N - 1];
    for (j=1; j < N; j++){

        CachedVals.push_back(temp);
        temp.multiplyBy(Equals[N - j - 1]);
        LessThan[N - j - 1].multiplyBy(CachedVals[j - 1]);
    }
    
    //reducing the whole temporary array b here to a single value that will be stored in temp[N - 1]; (Or reduction)
    for (int i=0;  i < N - 1; i++){
        
        //trying to do Or a + b - ab

        helib::Ctxt tmpA = LessThan[N - i - 1];         //store a in tmpA
        tmpA.multiplyBy(LessThan[N - i - 2]);           //now tmpA stores a*b
        LessThan[N - i - 2] += LessThan[N - i - 1];     //now temp[i + 1] stores a + b
        LessThan[N - i - 2] -= tmpA;                    //now temp[i + 1] stores a + b - ab
        Equals[i + 1].multiplyBy(Equals[i]);
                  
    }
    
    //reducing b' as in the paper pg 7 here using the equals array the reduced value will be in Equals[N - 1] (and reduction)    
    //calculating b Or b' as in the paper     

    Equals[N - 2] = Equals[N - 1];           //just to get a temporary for b'
    Equals[N - 1] += LessThan[0];            //adding b to b'
    LessThan[0].multiplyBy(Equals[N - 2]);   //multiplying b' to b
    Equals[N - 1] -= LessThan[0];            //subtracting addition to b * b' from b + b' to get b + b' - bb' (in other words b Or b')
    
    return Equals[N - 1];                    //return b Or b'
    
}


std::vector<std::vector<helib::Ctxt>> Evaluator::Evaluate(ModelOwner * Owner, Client &client){
    
    
    std::vector<TreeNode *>  decisionTrees = Owner->getDecisionTrees();
    std::vector<std::vector<std::vector<helib::Ctxt>>> encryptedThresholds = Owner->getencryptedThresholds();
    std::vector<std::vector<std::string>> featureMapping = Owner->getfeatureMapping();
    std::vector<std::vector<helib::Ctxt>> DecisionResults;
    
    tbb::parallel_for(size_t(0), static_cast<std::size_t>(Owner->getDecisionTrees().size()), [&](size_t numTrees) {  
        
        std::vector<std::vector<helib::Ctxt>> encryptedThresholdsInPreOrder = encryptedThresholds[numTrees];        
        std::vector<std::string> featureMappingInPreOrder = featureMapping[numTrees];
        std::vector<std::vector<helib::Ctxt>> clientThresholdAsTreeMapping;        
        std::map<std::string, std::vector<helib::Ctxt>> clientFeatureMap = client.getFeatureMap();
        
        for (int numThresholdsInTree = 0; numThresholdsInTree < featureMappingInPreOrder.size(); numThresholdsInTree++){
            
            clientThresholdAsTreeMapping.push_back(clientFeatureMap[featureMappingInPreOrder[numThresholdsInTree]]);            
        }
        
        //now we have the all the encryptedThresholds and the client's features duplicated as per the structure of the tree we can successfully call SecComp.
        //call secComp on each and every threshold of the tree and the corresponding client feature
         
        std::vector<helib::Ctxt> bValues (encryptedThresholdsInPreOrder.size(), helib::Ctxt(encryptedThresholdsInPreOrder[0][0].getPubKey()));
        int SIZE = encryptedThresholdsInPreOrder.size();    
        tbb::parallel_for(size_t(0), static_cast<std::size_t>(SIZE), [&](size_t treeSizeInner) {  
            
             helib::Ctxt bVal(encryptedThresholdsInPreOrder[0][0].getPubKey());
             bVal = SecComp(encryptedThresholdsInPreOrder[treeSizeInner], clientThresholdAsTreeMapping[treeSizeInner]);
             bValues[treeSizeInner] = bVal;
             
        });
        
         
        int index = 0;
        std::vector<helib::Ctxt> Label = recurseTree(decisionTrees[numTrees]->rightChild, bValues, &index);
        DecisionResults.push_back(Label);
        
     });

    return DecisionResults;

}

//Traversing the tree to get the label using the polynomial

 std::vector<helib::Ctxt> Evaluator::recurseTree(TreeNode * tree, std::vector<helib::Ctxt> &bValues, int * index){

    if (tree->leftChild == NULL && tree->rightChild == NULL){

          std::vector<helib::Ctxt> label;
          label = tree->encLabels;
          return label;
    }

    std::vector<helib::Ctxt> Left  = recurseTree(tree->leftChild, bValues, index);
    std::vector<helib::Ctxt> Right = recurseTree(tree->rightChild, bValues, index);

    Left[0].multiplyBy(bValues[*index]);   
    helib::Ctxt temp = bValues[*index]; 
    temp.addConstant(NTL::ZZ(1));
    Right[0].multiplyBy(temp);  
   
    std::vector<helib::Ctxt> result;

    result.push_back(Right[0]);
    result[0] += Left[0];
    
    *index = *index + 1;

    return result;

}
