#include "model-owner.h"
#include <stack>
#include <map>

//Methods for the model Owner
//The model Owner creates the tree from the input file

ModelOwner::ModelOwner(std::string _modelName){
    
    modelName = _modelName;
}


// A helper function to allocate memory for the tree node
TreeNode * CreateNode(std::string nodeName, long label, std::string feature, long threshold, EncInfo& info, int precision){ 
    
    TreeNode * tree = new TreeNode(info);
    if(tree == NULL)
    {   
        //malloc fail
        std::cout << "Failed to Malloc Tree Node in Create Node!\n" << std::endl;
        return NULL;
        
    }

    //assign the fields properly
    tree->leftChild = NULL; 
    tree->rightChild = NULL;

    tree->nodeName = nodeName;    
    tree->feature = feature;
    tree->threshold = threshold;
    tree->label = label;
    
    //encrypting the thresholds and the labels using info and the threshold, label values
    
    tree->thresholdBits.clear();
    for (long i = 0; i < precision; i++){
        tree->thresholdBits.push_back((threshold & (1 << i)) >> i);
    }
    
    tree->labelBits.clear();
    for (long i = 0; i < precision; i++){
        tree->labelBits.push_back((label & (1 << i)) >> i);
        
    }
    tree->labelBits.resize(info.context.ea->size());
    
    const helib::PubKey& public_key = *info.sk;
    const helib::EncryptedArray& ea = *(info.context.ea);
    
    tree->encThresholds.clear();
    for (int i = 0; i < tree->thresholdBits.size(); i++){
        helib::Ctxt encryptedBit(*info.sk);
        ea.encrypt(encryptedBit, public_key, std::vector<long>(info.context.ea->size(), tree->thresholdBits[i]));
        tree->encThresholds.push_back(encryptedBit);
    }
    
    tree->encLabels.clear();
    helib::Ctxt encryptedBit(*info.sk);
    ea.encrypt(encryptedBit, public_key, tree->labelBits);
    tree->encLabels.push_back(encryptedBit);
    
    return tree;
}
   
    
bool ModelOwner::makeDecisionTree(char * _filename, EncInfo& Info, int precision){
    
    std::stack <TreeNode*> TreeStack;
    std::ifstream in(_filename);
    
    if(!in) {        
        std::cout << "Cannot open input file.\n";
        return 1;        
    }
    
    std::string str;
    std::vector<std::string> lineVector;
    while (std::getline(in, str)){
        
        lineVector.push_back(str);
        
    }
    
    int lineCounter = 0;
    for (std::string line : lineVector){
        
        std::string word = "";
        std::vector<std::string> words;
        for(auto letter : line){
           
            if (letter == ' '){
                
                words.push_back(std::string(word));
                word = "";
                
            }
            else{
                
                word = word + letter;
            }
            
        }
        
        words.push_back(std::string(word));
        
        if (lineCounter == 1){
            
            for (int i=1; i < words.size(); i++){
                LabelMap[words[i]] = i - 1;
                InverseLabelMap[i - 1] = words[i];
            }
            
            
        }
        else if (lineCounter == 0){
            
            for (int i=1; i < words.size(); i++){
                FeaturesPresent.push_back(words[i]);
            }

        }

        int counter = 0;
        for (std::string word : words){
            
            if (word == "L" && counter == 1){
                //encountered a leaf node;
                //create a leaf tree node;
                TreeNode * newNode  = CreateNode(words[0], LabelMap[words[2]], "" , INT_MIN, Info, precision);
                TreeStack.push(newNode);
                break;
                
            }
            else if (word == "B" && counter == 1){
                
                //encountered a parent node
                //assign the left and right child appropriately
                TreeNode * newNode = CreateNode(words[0], INT_MIN, words[2], std::stol(words[3]), Info, precision);
                if (!TreeStack.empty()){
                    TreeNode * rightChild = TreeStack.top();
                    newNode->rightChild = rightChild;
                    TreeStack.pop();
                }
                if (!TreeStack.empty()){
                    TreeNode * leftChild = TreeStack.top();
                    newNode->leftChild = leftChild;    
                    TreeStack.pop();
                
                }              
                
                TreeStack.push(newNode);
                break;                
                
            }
            else if (word == "root" && counter == 0){
                
                //encountered the root node
                //assign left and right, add to the trees and clear the stack
                TreeNode * newNode = CreateNode(words[0], INT_MIN, "", INT_MIN, Info, precision);
                if (!TreeStack.empty()){
                    TreeNode * rightChild = TreeStack.top();
                    newNode->rightChild = rightChild;
                    TreeStack.pop();
                }
                if (!TreeStack.empty()){
                    TreeNode * leftChild  = TreeStack.top();
                    newNode->leftChild = leftChild;
                    TreeStack.pop();
                }                
                
                decisionTrees.push_back(newNode);
                break;               
                
            }            
            counter++;
            
        }
        lineCounter++;
        
    }
    
    return true;  
}

std::vector<std::vector<helib::Ctxt>> ModelOwner::getAllLabels(){

    return Labels;
}

//a print function for tree
void ModelOwner::PrintTree_Helper(TreeNode * head){                                                                        
                                                                                                               
if(head == NULL){                                                                                              
                                                                                                               
  return;                                                                                                      
}

if (head -> leftChild != NULL){
PrintTree_Helper(head -> leftChild);
}
if (head -> rightChild != NULL){
PrintTree_Helper(head -> rightChild);
}

std::string right = "NULL";
std::string left = "NULL";

if (head -> leftChild != NULL){
    left = head->leftChild->nodeName;

}
if (head -> rightChild != NULL){
    right = head->rightChild->nodeName;

}

std::cout << head->nodeName << std::endl;

}

void ModelOwner::PrintTree(){
    
   
    int j=1;
    for (TreeNode * decisionTree : decisionTrees){
        
        std::cout << std::endl;
        std::cout << "Printing Tree " << std::to_string(j) << " " << std::endl;
        PrintTree_Helper(decisionTree);
        std::cout << "completed" << std::endl;
        j++;
        
    }    
}

std::vector<TreeNode *> ModelOwner::getDecisionTrees(){
    
    return decisionTrees;
}
    
std::string ModelOwner::getModelName(){
    
    return modelName;
}

void PreOrder_helper(TreeNode * head, std::vector<std::vector<helib::Ctxt>> &treeThresholds, std::vector<std::string> &treeFeatures){
    
    if (head == NULL){
        return;
    }
    
   
        PreOrder_helper(head->leftChild, treeThresholds, treeFeatures);
        PreOrder_helper(head->rightChild, treeThresholds, treeFeatures);
         if (head->feature != ""){
        treeThresholds.push_back(head->encThresholds);
        treeFeatures.push_back(head->feature);
    }

    
}

void ModelOwner::getEncryptedThresholdsAndFeatureMapping(){
    
    for (TreeNode * decisionTree : decisionTrees){
        
        std::vector<std::vector<helib::Ctxt>> treeThresholds;
        std::vector<std::string> treeFeatures;
        PreOrder_helper(decisionTree, treeThresholds, treeFeatures);
        encryptedThresholds.push_back(treeThresholds);
        featureMapping.push_back(treeFeatures);        
    }
    
}

std::vector<std::vector<std::vector<helib::Ctxt>>> ModelOwner::getencryptedThresholds(){
    
    return encryptedThresholds;
    
}
std::vector<std::vector<std::string>> ModelOwner::getfeatureMapping(){
    
    return featureMapping;
    
}

std::map<std::string, long> ModelOwner::getLabelMap(){
    
    return LabelMap;
}

std::map<long, std::string> ModelOwner::getInverseLabelMap(){
    
    return InverseLabelMap;
}

std::vector<std::string> ModelOwner::getFeaturesPresent(){

    return FeaturesPresent;
}

void ModelOwner::deleteTree(TreeNode * head){
    
    if(head == NULL){
        return;
    }
    
    deleteTree(head->leftChild);
    deleteTree(head->rightChild);
    
    delete head;
    
}

ModelOwner::~ModelOwner(){}
