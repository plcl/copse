#ifndef CLIENT_H
#define CLIENT_H
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include <helib/FHE.h>
#include <helib/helib.h>
#include "EncodeInfo.h"
#include <map>



class Client{
    
private:
    std::vector<std::string> features;
    std::vector<long> thresholdVals;
    std::vector<std::vector<helib::Ctxt>> encThresholds;
    std::map<std::string, std::vector<helib::Ctxt>> featureToEncryptionMap;
    
public:
    Client(EncInfo& Info, std::vector<std::string> FeatureMap, int precision);
    std::vector<std::vector<helib::Ctxt>> getEncryptedThresholds();
    std::map<std::string, std::vector<helib::Ctxt>> getFeatureMap();
    std::vector<std::string> getFeatures();
    ~Client();   
    
}; 
#endif
