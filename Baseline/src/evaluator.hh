#include <array>
#include <vector>
#include <helib/helib.h>
#include "model-owner.h"
#include "client.h"
#include <vector>
#include <map>
#include <helib/FHE.h>
#include "tbb/tbb.h"

class Evaluator
{
private:
    
public:

    Evaluator();
    /**
     * Implements the SecComp protocol found here: https://eprint.iacr.org/2019/819.pdf
     * 
     * @param thresholdVec : fixedpoint threshold value for each feature, provided by model owner
     * @param featureVec : fixedpoint actual values for each feature, provided by data owner
     * 
     * @return an helib::Ctxt encrypted vector with a slot for each feature. The corresponding bit is
     *      '1' if the provided feature vector is less than the threshold, '0' otherwise.
     * 
     **/
    
    helib::Ctxt SecComp(
        std::vector<helib::Ctxt> featureVec,
        std::vector<helib::Ctxt> thresholdVec);
    
    //Evalutes the decision trees provided by the Model Owner
    std::vector<std::vector<helib::Ctxt>> Evaluate(ModelOwner * Owner, Client &client);
    
    //Computes the polynomial from the decision values
    std::vector<helib::Ctxt> recurseTree(TreeNode * tree, std::vector<helib::Ctxt> &bValues, int * index);
    
};


