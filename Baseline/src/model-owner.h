#ifndef MODEL_OWNER_H
#define MODEL_OWNER_H
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include <helib/FHE.h>
#include <helib/helib.h>
#include "EncodeInfo.h"
#include <map>

//structure that represents a decision tree node
typedef struct Tree{    
    
    std::string nodeName;    
    std::string feature;
    long threshold;
    long label;
    std::vector<long> thresholdBits;
    std::vector<long> labelBits;
    std::vector<helib::Ctxt> encThresholds;
    std::vector<helib::Ctxt> encLabels;
    struct Tree * leftChild;
    struct Tree * rightChild;
    
    Tree(EncInfo& Info){
        //just making sure everything is initialized to something, its just needed to make sure conditional jump errors don't occur.
        threshold = 0;
        label = 0;
        feature = "";
        nodeName = "";
        leftChild = NULL;
        rightChild = NULL;
        thresholdBits.push_back(0);
        labelBits.push_back(0);
        helib::Ctxt encryptedThresholds(*Info.sk);
        encThresholds.push_back(encryptedThresholds);
        helib::Ctxt encryptedLabels(*Info.sk);
        encLabels.push_back(encryptedLabels);
    }
    
    
    
}TreeNode;

//class for the model owner
class ModelOwner{
    
private: 
    std::string modelName;
    std::vector<TreeNode *> decisionTrees;
    std::vector<std::vector<std::vector<helib::Ctxt>>> encryptedThresholds;
    std::vector<std::vector<std::string>> featureMapping;
    std::vector<std::vector<helib::Ctxt>> Labels;
    std::map<std::string, long> LabelMap;
    std::map<long, std::string> InverseLabelMap;
    std::vector<std::string> FeaturesPresent;
    
public:
    ModelOwner(std::string modelName);
    bool makeDecisionTree(char * filename, EncInfo& Info, int precision);
    std::vector<TreeNode *> getDecisionTrees();
    std::string getModelName();
    void PrintTree();
    void PrintTree_Helper(TreeNode * head);
    void getEncryptedThresholdsAndFeatureMapping();
    std::map<std::string, long> getLabelMap();
    std::map<long, std::string> getInverseLabelMap();
    std::vector<std::string> getFeaturesPresent();
    friend void PreOrder_helper(TreeNode * head, std::vector<std::vector<helib::Ctxt>> &treeThresholds, std::vector<std::string> &treeFeatures);
    std::vector<std::vector<std::vector<helib::Ctxt>>> getencryptedThresholds();
    std::vector<std::vector<std::string>> getfeatureMapping();
    std::vector<std::vector<helib::Ctxt>> getAllLabels();
    void deleteTree(TreeNode * head);
    ~ModelOwner();   
    
};
#endif
