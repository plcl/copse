#include "DecisionTree.h"


DecisionTree::DecisionTree(int _ID){
    
    ID = _ID;
    
}

void DecisionTree::Print(){
    
    for (auto feature : plaintextFeatures)
            std::cout << feature << " ";
    std::cout << std::endl;
    for (auto threshold : plaintextThresholds)
            std::cout << threshold << " ";
    std::cout << std::endl;
    for (auto labels : plaintextLables)
            std::cout << labels << " ";
    std::cout << std::endl;
}
                                                           
