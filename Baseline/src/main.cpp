#include <iostream>
#include <vector>
#include "model-owner.h"
#include "client.h"
#include "evaluator.hh"
#include <chrono>
#include <NTL/BasicThreadPool.h>


int power2(int superscript)
{  
    int power = 1;
 
    for(int i = 0; i < superscript; i++)
        power *= 2;    
 
    return power;
}

int main(int argc, char ** argv){

    if (argc < 4){
        
        std::cout << "Wrong number of arguments!" << std::endl;
        std::cout << "Usage: ";
        std::cout << "./executable " << "inputFile.tree " << "threads " << "precision " << std::endl; 
        exit(1);
    }

    char * numThreads = argv[2];
    int threadsToUse = std::stoi(numThreads);
    
    char * setPrecision = argv[3];
    int precision = std::stoi(setPrecision);
    
    tbb::task_scheduler_init init(threadsToUse);
    unsigned long p = 2;
    unsigned long m = 32109;
    unsigned long bits = 500;
    unsigned long r = 1;

    unsigned long sec = 128;
    unsigned long cols = 3;
    unsigned long minslots = 0;

    auto start = std::chrono::high_resolution_clock::now();     
    
    std::cout << "finding a suitable m value" << std::endl; 
    m = helib::FindM(sec, bits, cols, p, minslots, 0, 0);
    std::cout << "Done!" << std::endl;

    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::chrono::duration<long, std::micro> execTime = duration; 
    std::cout << "Took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;                                                                                                                         
    
    start = std::chrono::high_resolution_clock::now();
    std::cout << "Making the Encode Info context" << std::endl;
    EncInfo info(m, p, r, bits, cols);
    std::cout << "Done!" << std::endl;
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    execTime = duration; 
    std::cout << "Took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;
    
    char * filename = argv[1];
    FILE * File = fopen(filename, "r");
    if(File == NULL){
        
        exit(0);
    }
                                                                        
    start = std::chrono::high_resolution_clock::now();
    std::cout << "New Model Owner" << std::endl;     
    ModelOwner * Owner = new ModelOwner(std::string(filename));
    std::cout << "Done!" << std::endl;
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    execTime = duration; 
    std::cout << "Took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;

    start = std::chrono::high_resolution_clock::now();  
    std::cout << "Make the decision tree using the file and the Context Info" << std::endl;
    Owner->makeDecisionTree(filename, info, precision);
    std::cout << "Done!" << std::endl;
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    execTime = duration; 
    std::cout << "Took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;
    

    std::cout << "getting the encrypted thresholds and features" << std::endl; 
    Owner->getEncryptedThresholdsAndFeatureMapping();
    std::cout << "Done!" << std::endl;
    std::cout << std::endl;
    
    std::vector<std::string> FeatureMap = Owner->getFeaturesPresent();

    start = std::chrono::high_resolution_clock::now();  
    std::cout << "Making the Client" << std::endl;
    Client client(info, FeatureMap, precision);
    std::cout << "Done!" << std::endl;
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    execTime = duration; 
    std::cout << "Took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;
    

    std::cout << "making the evaluator" << std::endl;
    Evaluator evaluator;
    std::cout << "Done!" << std::endl;
    std::cout << std::endl;
    
    std::cout << "running the evaluator, SecComp and Polynomial" << std::endl;
    start = std::chrono::high_resolution_clock::now();      
    std::vector<std::vector<helib::Ctxt>> DecisionResults = evaluator.Evaluate(Owner, client);
    stop = std::chrono::high_resolution_clock::now();
    std::cout << "Done!" << std::endl;
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    execTime = duration; 
    std::cout << "Evaluator took " << execTime.count() << " microseconds"  << std::endl;
    std::cout << std::endl;
    
    std::vector<TreeNode *> decisionTrees = Owner->getDecisionTrees();
    
    std::map<long, std::string> LabelMap = Owner->getInverseLabelMap();
    
    const helib::EncryptedArray& ea = *(info.context.ea);
    std::cout << "Printing the Decision Results, i.e, the Labels chosen by each Tree." << std::endl; 
    for(int tree=0; tree < decisionTrees.size(); tree++){
        
        std::vector<helib::Ctxt> Label;
        Label = DecisionResults[tree];
        std::vector<long> LabelBits;
        for(int l=0; l < Label.size(); l++){
            std::vector<long> decryptedLabels(info.context.ea->size());
            ea.decrypt(Label[l], *info.sk, decryptedLabels);
            LabelBits = decryptedLabels;
            
        }
        std::cout << "Printing the Label chosen by the Polynomial for TREE " + std::to_string(tree + 1) << std::endl;
        long decimalVal = 0;
        int bit = 0;
        for (int label=0; label < LabelBits.size(); label++){
            
            bit = LabelBits[label];
            decimalVal += bit * power2(label);
        }
        std::cout << "The label chosen is: " << LabelMap[decimalVal] << std::endl;
        std::cout << std::endl;        
    }    
    
    std::cout << "Freeing memory, kindly do not terminate program..." << std::endl;    
    for (int i=0; i < decisionTrees.size(); i++){
        std::cout << "Freeing tree " << std::to_string(i) << std::endl; 
        Owner->deleteTree(decisionTrees[i]);
    }
    std::cout << "Done Freeing Decision Trees!" << std::endl;
    
    std::cout << "Freeing the model Owner" << std::endl;
    delete Owner;
    std::cout << "Owner Freed!" << std::endl;    
    
    std::cout << "Program completed! Terminating..." << std::endl;

    std::cerr << execTime.count();    
    
    
}
