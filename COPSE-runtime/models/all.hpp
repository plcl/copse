#pragma once

#include "depths/all.hpp"
#include "precisions/all.hpp"
#include "widths/all.hpp"
#include "real_world/all.hpp"
