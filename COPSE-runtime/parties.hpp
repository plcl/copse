#pragma once

#include "data-owner.hpp"
#include "model-owner.hpp"

class Maurice : public ModelOwner
{
  private:
    std::vector<PtxtModelDescription> models;
    void loadModels();
    int cur;

  protected:
    virtual PtxtModelDescription plaintext_model();

  public:
    Maurice(const EncInfo &info) : ModelOwner(info), cur(0)
    {
        loadModels();
    }

    int numModels();
};

class Danielle : public DataOwner
{
  private:
    std::vector<ptxt_vec> features;
    int cur;

  protected:
    virtual ptxt_vec plaintext_features();
    virtual void accept_inference(ctxt_vec);
    virtual void accept_inference(zzx_vec);

  public:
    void loadFeatures(int);
    Danielle(EncInfo &info) : DataOwner(info), cur(0)
    {
        loadFeatures(8);
    }
};
