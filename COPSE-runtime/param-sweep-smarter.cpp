#include "parties.hpp"
#include "vectrees.hpp"

#include <chrono>
#include <functional>

struct annotated_ctxt
{
    ctxt_vec vec;
    int depth;

    annotated_ctxt(ctxt_vec vec) : vec(vec), depth(0)
    {
    }
};

void mult(annotated_ctxt &a1, annotated_ctxt a2)
{
    a1.vec.multiplyBy(a2.vec);
    a1.depth = 1 + std::max(a1.depth, a2.depth);
}

annotated_ctxt annotated_mat_mul(std::vector<annotated_ctxt> mat, std::vector<annotated_ctxt> rots)
{
    std::vector<ctxt_vec> raw_mat, raw_rots;
    int maxDepth = 0;
    for (auto diag : mat)
    {
        raw_mat.push_back(diag.vec);
        if (diag.depth > maxDepth)
            maxDepth = diag.depth;
    }

    for (auto rot : rots)
    {
        raw_rots.push_back(rot.vec);
        if (rot.depth > maxDepth)
            maxDepth = rot.depth;
    }

    annotated_ctxt result(mat_mul(raw_mat, raw_rots));
    result.depth = maxDepth + 1;
    return result;
}

std::vector<annotated_ctxt> generate_annotated_rotations(annotated_ctxt vec, int cols,
                                                         std::shared_ptr<const helib::EncryptedArray> ea)
{
    auto rotations = generate_rotations(vec.vec, cols, ea);
    std::vector<annotated_ctxt> annotated_rotations;
    for (auto rot : rotations)
    {
        annotated_ctxt annotated_rot(rot);
        annotated_rot.depth = vec.depth;
        annotated_rotations.push_back(annotated_rot);
    }

    return annotated_rotations;
}

void add(annotated_ctxt &a1, annotated_ctxt a2)
{
    a1.vec += a2.vec;
    a1.depth = std::max(a1.depth, a2.depth);
}

void add(annotated_ctxt &a1, NTL::ZZX a2)
{
    a1.vec.addConstant(a2);
}

std::vector<annotated_ctxt> prefix_mult(std::vector<annotated_ctxt> vals)
{
    int N = vals.size();
    std::vector<annotated_ctxt> acc = vals, cur = vals;
    std::vector<int> depths(acc.size());

    for (int i = 1; i < N; i <<= 1)
    {
        acc = cur;
        NTL_EXEC_INDEX(N - i, j)
        {
            cur[j] = acc[j];
            mult(cur[j], acc[j + 1]);

            depths[j]++;
        }
        NTL_EXEC_INDEX_END
    }

    for (auto d : depths)
        std::cerr << d << " ";
    std::cerr << "\n";

    return cur;
}

annotated_ctxt reduce(std::vector<annotated_ctxt> vals,
                      std::function<annotated_ctxt(annotated_ctxt, annotated_ctxt)> func, int start, int end)
{

    if (end - start == 0 || vals.size() == 0)
        throw new std::runtime_error("Cannot reduce size-0 vector");
    if (end - start == 1)
        return vals[start];

    int mid = start + (end - start) / 2;

    auto lhs = reduce(vals, func, start, mid);
    auto rhs = reduce(vals, func, mid, end);

    auto answer = func(lhs, rhs);
    return answer;
}

annotated_ctxt compare(std::vector<annotated_ctxt> thresholds, std::vector<annotated_ctxt> features)
{
    int N = thresholds.size();
    std::vector<annotated_ctxt> L, Q;

    std::cout << "Initial noises: " << thresholds[0].vec.capacity() << "; " << features[0].vec.capacity() << "\n";

    for (int i = 0; i < N; i++)
    {
        annotated_ctxt _l = features[i];
        annotated_ctxt _q = features[i];

        add(_l, NTL::ZZX(1));
        mult(_l, thresholds[i]);

        add(_q, NTL::ZZX(1));
        add(_q, thresholds[i]);

        L.push_back(_l);
        Q.push_back(_q);
    }

    std::cout << "L, Q noises: " << L[0].vec.capacity() << "; " << Q[0].vec.capacity() << "\n";

    std::vector<annotated_ctxt> E = prefix_mult(Q);

    std::cout << "Max E noise: " << E[0].vec.capacity() << "\n";

    NTL_EXEC_INDEX(N - 1, i)
    mult(E[i + 1], L[i]);
    NTL_EXEC_INDEX_END

    std::cout << "Max E + 1 noise: " << E[1].vec.capacity() << "\n";

    annotated_ctxt result = reduce(
        E,
        [](annotated_ctxt lhs, annotated_ctxt rhs) {
            annotated_ctxt prod = lhs;
            mult(prod, rhs);
            add(lhs, rhs);
            add(lhs, prod);

            return lhs;
        },
        0, E.size());

    annotated_ctxt prod = result;

    std::cout << "Reduced (mult) noise: " << prod.vec.capacity() << "\n";

    mult(prod, L[N - 1]);
    add(result, L[N - 1]);
    add(result, prod);

    return result;
}

CtxtModelDescription *gen_model(int depth, int fxp_bits, int branching, int quantized_branching, int num_slots)
{
    std::vector<ptxt_vec> ptxt_thresholds;
}

struct model_size
{
    int fixpoint_precision = 8;
    int model_depth = 4;
    int quantized_branching = 20;
    int branching = 15;
    int width = 17;
};

struct annotated_model_info
{
    std::vector<annotated_ctxt> thresholds;
    std::vector<annotated_ctxt> d2b_matrix;
    std::vector<std::vector<annotated_ctxt>> level_matrices;
    std::vector<annotated_ctxt> level_masks;

    annotated_model_info(model_size sz, EncInfo &info)
    {
        ptxt_vec ptxt;
        ptxt.resize(info.nslots);
        for (int i = 0; i < sz.fixpoint_precision; i++)
            thresholds.push_back(encrypt_vector(info, ptxt));
        for (int i = 0; i < sz.quantized_branching; i++)
            d2b_matrix.push_back(encrypt_vector(info, ptxt));

        for (int d = 0; d < sz.model_depth; d++)
        {
            std::vector<annotated_ctxt> level_matrix;
            for (int i = 0; i < sz.branching; i++)
                level_matrix.push_back(encrypt_vector(info, ptxt));
            level_matrices.push_back(level_matrix);
            level_masks.push_back(encrypt_vector(info, ptxt));
        }
    }
};

int main(int argc, char **argv)
{

    using ms = std::chrono::milliseconds;
    using clock = std::chrono::high_resolution_clock;

    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0] << " [sec] [bits] [cols]\n";
        return -1;
    }

    const unsigned long sec = std::atoi(argv[1]);
    const unsigned long bits = std::atoi(argv[2]);
    const unsigned long cols = std::atoi(argv[3]);

    constexpr unsigned long p = 2;
    constexpr unsigned long r = 1;

    std::cerr << "Building params...\n";
    const unsigned long m = helib::FindM(sec, bits, cols, p, 0, 0, 0);

    EncInfo *info;

    try
    {
        info = new EncInfo(m, p, r, bits, cols);
    }
    catch (std::exception e)
    {
        return 1;
    }

    ptxt_vec ptxt1;
    ptxt1.resize(info->nslots);

    ctxt_vec ctxt1 = encrypt_vector(*info, ptxt1);
    ctxt_vec ctxt2 = encrypt_vector(*info, ptxt1);

    // ctxt_mat rots1, rots2;
    // for (int i = 0; i < 20; i++)

    auto rots1 = generate_rotations(ctxt1, 5, info->context.ea);
    auto rots2 = generate_rotations(ctxt2, 5, info->context.ea);

    std::cout << "At level -1: " << ctxt1.capacity() << "\n";

    for (int i = 0; i < 20; i++)
    {
        auto response = mat_mul(rots1, rots2);
        std::cout << "At level " << i << ": " << response.capacity() << "\n";
        rots1 = generate_rotations(response, 5, info->context.ea);
    }

    return 0;

    model_size depth4_sz;
    annotated_model_info depth4_model(depth4_sz, *info);

    ptxt_vec ptxt;
    ptxt.resize(info->nslots);
    std::vector<annotated_ctxt> features;
    for (int i = 0; i < depth4_sz.fixpoint_precision; i++)
        features.push_back(encrypt_vector(*info, ptxt));

    // Perform comparison step
    auto decisions = compare(depth4_model.thresholds, features);
    std::cout << "Decision noise: " << decisions.vec.capacity() << "\n";
    auto decision_rots = generate_annotated_rotations(decisions, depth4_model.d2b_matrix.size(), info->context.ea);

    auto reshaped_branches = annotated_mat_mul(depth4_model.d2b_matrix, decision_rots);
    std::cout << "Reshaped branches noise: " << reshaped_branches.vec.capacity() << "\n";
    auto reshaped_rots =
        generate_annotated_rotations(reshaped_branches, depth4_model.level_matrices[0].size(), info->context.ea);

    std::vector<annotated_ctxt> xored_masks;
    for (int i = 0; i < depth4_sz.model_depth; i++)
    {
        auto level_mat = depth4_model.level_matrices[i];
        auto mask = depth4_model.level_masks[i];

        auto slots = annotated_mat_mul(level_mat, reshaped_rots);
        add(slots, mask);
        std::cout << "Slots @" << i << " noise: " << slots.vec.capacity() << "\n";
        xored_masks.push_back(slots);
    }

    auto inference = reduce(
        xored_masks,
        [](auto lhs, auto rhs) {
            mult(lhs, rhs);
            return lhs;
        },
        0, xored_masks.size());

    std::cout << inference.depth << "\n";
    std::cout << "(Final noise = " << inference.vec.capacity() << ")\n";

    return 0;
}