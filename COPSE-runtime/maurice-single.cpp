#if !defined MODEL_NAME || !defined MODEL_INC
#error No model name specified!!
#endif

#include "parties.hpp"

#define _STR(x) #x
#define STR(x) _STR(x)

#include STR(MODEL_INC)

void Maurice::loadModels()
{
}

PtxtModelDescription Maurice::plaintext_model()
{
    return MODEL_NAME();
}

int Maurice::numModels()
{
    return 1;
}
