#include "parties.hpp"

#include "models/all.hpp"

void Maurice::loadModels()
{
    models = {depth4(), depth5(), depth6(),   width55(),  width78(),   width677(),
              prec8(),  prec16(), soccer_5(), income_5(), soccer_15(), income_15()};
}

PtxtModelDescription Maurice::plaintext_model()
{
    return models[cur++];
}

int Maurice::numModels()
{
    return models.size();
}
