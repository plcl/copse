secs=(128 192 256 512)
bits=(100 200 300 400 500 600 700 800)
cols=(2 3)

for sec in ${secs[*]}; do
for bit in ${bits[*]}; do
for col in ${cols[*]}; do
    echo $sec $bit $col;
    ./sweep $sec $bit $col >> sweeps.csv;
done
done
done
done
