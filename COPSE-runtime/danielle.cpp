#include "parties.hpp"

extern int QUERIES_PER_MODEL;
extern int REPEAT_QUERIES;

void Danielle::loadFeatures(int bits)
{
    features.clear();
    for (int i = 0; i < QUERIES_PER_MODEL; i++)
    {
        long x, y;

        NTL::RandomBits(x, bits);
        NTL::RandomBits(y, bits);
        for (int j = 0; j < REPEAT_QUERIES; j++)
            features.push_back({x, y});
    }
    cur = 0;
}

ptxt_vec Danielle::plaintext_features()
{
    cur = cur % features.size();
    return features[cur++];
}

void Danielle::accept_inference(ctxt_vec inference)
{
    ptxt_vec ptxt_inference = decrypt_vector(info, inference);
    print_vec(std::cerr, ptxt_inference);
}

void Danielle::accept_inference(zzx_vec inference)
{
    ptxt_vec ptxt_inference = decode_vector(info, inference);
    print_vec(std::cerr, ptxt_inference);
}
