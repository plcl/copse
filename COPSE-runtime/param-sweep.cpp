#include "model-owner.hpp"
#include "models/real_world/soccer_5.out"
#include "vectrees.hpp"
#include <NTL/BasicThreadPool.h>
#include <chrono>

int main(int argc, const char *const argv[])
{

    using ms = std::chrono::milliseconds;
    using clock = std::chrono::high_resolution_clock;

    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0] << " [sec] [bits] [cols]\n";
        return -1;
    }

    const unsigned long sec = std::atoi(argv[1]);
    const unsigned long bits = std::atoi(argv[2]);
    const unsigned long cols = std::atoi(argv[3]);

    constexpr unsigned long p = 2;
    constexpr unsigned long r = 1;

    std::cerr << "Building params...\n";
    const unsigned long m = helib::FindM(sec, bits, cols, p, 0, 0, 0);

    EncInfo *info;

    try
    {
        info = new EncInfo(m, p, r, bits, cols);
    }
    catch (std::exception e)
    {
        return 1;
    }

    ptxt_vec ptxt1, ptxt2;
    ptxt1.resize(info->nslots);
    ptxt2.resize(info->nslots);

    auto ctxt1 = encrypt_vector(*info, ptxt1);
    auto ctxt2 = encrypt_vector(*info, ptxt2);

    int i;
    float oldCap = 0;

    std::cerr << "Getting rotation time...\n";
    auto t1 = clock::now();
    info->context.ea->rotate(ctxt1, 1);
    auto t2 = clock::now();

    auto rot_time = std::chrono::duration_cast<ms>(t2 - t1).count();

    std::cerr << "Getting multiplicative depth and time...\n";
    t1 = clock::now();
    for (i = 0; i < 100; i++)
    {
        auto cap = ctxt2.capacity();
        std::cout << cap << "\n";
        if (cap < 0)
            break;

        ctxt2 *= (ctxt1);
        // std::cerr << "Round " << i + 1 << "(" << oldCap - cap << ", " << cap << ")\n";
        oldCap = cap;
    }
    t2 = clock::now();

    auto all_mul_time = std::chrono::duration_cast<ms>(t2 - t1).count();
    std::cerr << "Done! (Reached depth of " << i - 1 << ")\n";

    std::cout << sec << "," << bits << "," << cols << "," << m << "," << i - 1 << "," << info->nslots << "," << rot_time
              << "," << all_mul_time << "\n";

    return 0;
}
