#include "data-owner.hpp"
#include "model-owner.hpp"
#include "parties.hpp"
#include "sally-server.hpp"

#include <NTL/BasicThreadPool.h>
#include <chrono>
#include <utility>
#include <memory>

int QUERIES_PER_MODEL = 3;
int REPEAT_QUERIES = 1;

// void print_mat(const EncInfo &info, const ctxt_mat mat, int sz)
// {
//     std::cerr << "------------------------------\n";
//     for (auto vec : mat)
//     {
//         std::vector<long> decrypted;
//         info.context.ea->decrypt(vec, *info.sk, decrypted);
//         for (int i = 0; i < sz; i++)
//             std::cerr << decrypted[i] << "";
//         std::cerr << "\n";
//     }
//     std::cerr << "------------------------------\n";
// }

// void print_vec(const EncInfo &info, const ctxt_vec vec, int sz)
// {
//     std::cerr << "------------------------------\n";
//     std::vector<long> decrypted;
//     info.context.ea->decrypt(vec, *info.sk, decrypted);
//     for (int i = 0; i < sz; i++)
//         std::cerr << decrypted[i] << "";
//     std::cerr << "\n------------------------------\n";
// }

void RunProtocol(EncInfo &info)
{
    using clock = std::chrono::high_resolution_clock;
    using ms = std::chrono::milliseconds;
    std::vector<std::pair<CtxtModelDescription *, int>> load_times;

    std::cerr << "Initializing parties...\n";
    auto maurice = std::make_shared<Maurice>(info);
    auto danielle = std::make_shared<Danielle>(info);

    SallyServer *sally = new SallyServer(danielle, maurice, info.context.getEA());

    for (int i = 0; i < maurice->numModels(); i++)
    {
        std::cerr << "Model #" << i + 1 << "/" << maurice->numModels() << "\n";
        auto t1 = clock::now();
        sally->LoadModel();
        auto t2 = clock::now();
        load_times.push_back(std::make_pair(sally->model, std::chrono::duration_cast<ms>(t2 - t1).count()));

        danielle->loadFeatures(sally->model->bits);

        // if (sally->model->name == "prec16")
        // danielle->loadFeatures(16);
        // else
        // danielle->loadFeatures(8);
        for (int j = 0; j < QUERIES_PER_MODEL; j++)
        {
            for (int k = 0; k < REPEAT_QUERIES; k++)
            // for (int k = 0; k < 2; k++)
            {
                std::cerr << "Query #" << j + 1 << "/" << QUERIES_PER_MODEL << " (" << k + 1 << "/" << REPEAT_QUERIES
                          << ")\n";
                // std::cerr << "Query #" << j + 1 << "/" << QUERIES_PER_MODEL << " (" << k + 1 << "/" << 2 << ")\n";
                sally->ExecuteQuery();
            }
        }
    }

    std::cout << "name,depth,width,quantized,duration\n";
    for (auto model : load_times)
    {
        auto m = model.first;
        auto t = model.second;

        std::cout << m->name << "," << m->depth << "," << m->width << "," << m->quantized << "," << t << "\n";
    }
}

int main(int argc, char **argv)
{

#ifdef VECTREE_THREADED
    NTL::SetNumThreads(256);
#endif
    unsigned long p = 2;
    unsigned long m = 32109;
    unsigned long bits = 700;
    unsigned long r = 1;

    unsigned long sec = 128;
    unsigned long cols = 2;
    unsigned long minslots = 0;

    std::cerr << "Finding a suitable m...\n";
    m = helib::FindM(sec, bits, cols, p, minslots, 0, 0);
    std::cerr << "Found m = " << m << "\n";

    auto t1 = std::chrono::high_resolution_clock::now();
    EncInfo info(m, p, r, bits, cols);
    auto t2 = std::chrono::high_resolution_clock::now();

    std::cerr << "Initialized security parameters; took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "ms\n";

    std::cerr << info.nslots << " slots\n";

    if (argc > 1)
        QUERIES_PER_MODEL = std::atoi(argv[1]);
    if (argc > 2)
        REPEAT_QUERIES = std::atoi(argv[2]);

    std::cerr << "Running " << QUERIES_PER_MODEL << " queries on each model, repeated " << REPEAT_QUERIES << " times\n";
    RunProtocol(info);

    return 0;
}
