from json import dumps
from sys import argv
import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import load_iris

precision = 1 << 16

def serialize(model, labels, path):
    # Traversal helper for decision trees. Builds a string with the serialization
    def dfs(node_idx):
        if tree.children_left[node_idx] != tree.children_right[node_idx]:   # This is an internal brance node
            return "Branch {threshold = Threshold {feat = F %d, val = %d}, left = %s, right = %s}" % (
                tree.feature[node_idx], int(tree.threshold[node_idx] * (precision / thresh_max)),
                dfs(tree.children_left[node_idx]), dfs(tree.children_right[node_idx]))
        else:  # This is a leaf node
            return "Label (L %d)" % dec_tree.classes_[np.argmax(tree.value[node_idx])]

    with open(path, "w") as fo:
        # First write the labels to the output file
        fo.write(dumps(list(labels.categories)) + "\n")

        # Calculate the maximum threshold value for the tree.
        thresh_max = max([max(dec_tree.tree_.threshold) for dec_tree in model.estimators_])
        
        # Recursively serialize each tree and write it to a newline in the file
        for dec_tree in model.estimators_:
            tree = dec_tree.tree_
            fo.write(dfs(0) + "\n")  # 0 is the root's node idx

if __name__ == "__main__":
    # Load in data and labels
    if len(argv) == 1:
        print("Usage: python create_rf <.csv file path to model as random forest> <Column name with labels> <.mdl file to which the serialization is written>")
        print("No arguments given. Using default options.")
        iris = load_iris()
        df = pd.DataFrame(iris.data, columns=iris.feature_names)
        labels = pd.Categorical.from_codes(iris.target, iris.target_names)
        path = "serialization.mdl"
    elif len(argv) == 4:
        df = pd.read_csv(argv[1])
        
        # Drop any incomplete rows
        for col in df:
            df[col].replace("", np.nan, inplace=True)
            df.dropna(subset=[col], inplace=True)

        # Don't include labels in the model's data frame
        labels = pd.Categorical(df.pop(argv[2]))
        path = argv[3]

        # Encode the data set so strings are properly handled
        le = preprocessing.LabelEncoder()
        for col in df:
            df[col] = le.fit_transform(df[col])
    else:
        print("Usage: python create_rf <.csv file path to model as random forest> <Column name with labels> <.txt file to which the serialization is written>")
        exit()
    
    # Split data to yield 30% test size
    train, test, train_labels, test_labels = train_test_split(df, labels, stratify = labels, test_size = 0.3, random_state = 41599)

    # Imputation of missing values if any
    train = train.fillna(train.mean())
    test = test.fillna(test.mean())

    # Create the model with 25 trees
    model = RandomForestClassifier(n_estimators = 25, random_state = 41599, max_features = 'sqrt', max_depth = 5, n_jobs = -1)

    # Fit on training data
    model.fit(train, train_labels)

    # Show model accuracy on test set
    train_predictions = model.predict(train)
    test_predictions = model.predict(test)
    train_labels = np.array(train_labels)
    test_labels = np.array(test_labels)

    # I know there's a method for this, but it was throwing a weird error and this seemed like the quickest fix at the time...
    print("Trainset accuracy = %f" % (sum([1 if train_predictions[i] == train_labels[i] else 0 for i in range(len(train_labels))]) / len(train_labels)))
    print("Testset accuracy = %f" % (sum([1 if test_predictions[i] == test_labels[i] else 0 for i in range(len(test_labels))]) / len(test_labels)))

    # Serialize the tree and write the result to the path specified.
    serialize(model, labels, path)
