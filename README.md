# COPSE: Vectorized Evaluation of Secure Decision Forests
[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg)](http://opensource.org/licenses/MIT)

## Contents
* [Introduction](#introduction)
* [Requirements](#requirements)
    * [Software](#software)
    * [Hardware](#hardware)
* [Usage](#usage)
    * [Producing a vectorized model](#producing-a-vectorized-model)
    * [Running secure vectorized inference](#running-secure-vectorized-inference)
    * [Running baseline implementation](#running-baseline-implementation)
* [Demo](#demo)
## Introduction
This repository contains the source code for the COPSE system described in the paper **Vectorized Evaluation of Secure Decision Forests**.
This includes:

* The COPSE compiler to generate vectorized code for trained decision forest models
* The COPSE runtime system to encrypt and execute vectorized models
* Our implementation of the [baseline](https://eprint.iacr.org/2019/819.pdf) against which COPSE is compared


## Requirements
### Software
* The COPSE compiler uses [Haskell stack](https://docs.haskellstack.org/en/stable/README/) for its build system.
* The COPSE runtime uses [HElib](https://github.com/homenc/HElib) as its underlying FHE implementation, as well as some functionality from [NTL](https://libntl.org/).
* The baseline implementation additionally uses Intel's [Thread Building Blocks](https://github.com/wjakob/tbb.git) (TBB) library for multithreading

COPSE expects all libraries to be installed under `~/.local` (i.e., the header files should be placed under `~/.local/include/` and the lib files should be under `~/.local/lib/`)
### Hardware
* At least 32 GB of RAM is required to run the large models (`income15` and `soccer15`)
* It is recommended to have at least 32 CPU cores to take full advantage of multithreading in the COPSE runtime.

## Usage
### Producing a vectorized model
*Building the compiler*

The source code for the COPSE compiler is in the <COPSE-compiler> directory.
It can be build using Haskell stack by running `cd COPSE-compiler && stack build --fast && stack install`.
This will build the `copse` binary and place it on your path.

*Serializing a trained model*

A trained decision forest must first be serialized to a `.mdl` file, as shown in the examples [here](serialized-models).
The serialization format is as follows:

* The first line of the file is a list of label names used, with each label name surrounded by double quotes, e.g. `["label1", "label2", "label3"]`
* Each subsequent line in the file contains a single tree, serialized recursively. 
A leaf node of the tree pointing to the label with index `N` is represented as `Label (L N)`.
A branch node representing the decision `x_M < P` is represented as:
```
Branch {threshold = Threshold {feat = F M, val = P}, left = S1, right = S2}
```
where `S1` and `S2` are serializations of the left and right subtrees (false and true), respectively.

This [script](datasets/train_model.py) shows an example of training a decision forest model and serializing it appropriately to a file.

*Compiling a serialized model*

Once a model has been serialized, the COPSE compiler can be invoked to generate a vectorized representation on stdout.
COPSE currently supports two modes of compilation: it can directly vectorize models or it can apply some basic optimization passes (such as common subexpression elimination) to the vectorized code.
Assuming the compiler has already been built and installed, run 
```
copse (compile|compileOpt) /path/to/model.mdl > vectorized.out
```
to compile the model and save the generated code in `vectorized.out`.
Use the `compile` option to compile without optimizations and `compileOpt` to compile with optimizations.

### Running secure vectorized inference

The <COPSE-runtime> directory contains code for loading models and securely executing inference queries.
This code must be built into various *benchmarks* which can then be executed.

Each benchmark does the following steps:

1. Loads and (optionally) encrypts a set of compiled, vectorized models
1. Randomly generates a configurable number of inference queries (each consisting of a feature vector)
1. Encrypts each query and sends it to the model for inference a configurable number of times

The directory also contains a [Makefile](COPSE-runtime/Makefile) that can be used to build these benchmarks.
The Makefile accepts the following options:

* `THREADING=on/off` (default `off`): enables multithreading for the compiled benchmark when `on`
* `PTXTMOD=on/off` (default `off`): disables encrypting the models when `on`
* `DEBUG=on/off` (default `off`): builds a debug version of the benchmark binary when `on`
* `PROF=on/off` (default `off`): builds a profiled version of the benchmark binary when `on`

*Building pre-prepared benchmarks*

This repository also contains precompiled versions of all the benchmarks used for evaluation in the paper, as well as build targets in the [Makefile](COPSE-runtime/Makefile) to build subsets of these benchmarks. 
These are as follows:

| Build target | Benchmarks run  |
| ------------ | --------------- |
| sally        | all twelve      |
| sally-small  | all but income_15 and soccer_15 |
| sally-tiny   | depth4, depth5, width55, prec8 |

*Building benchmark for custom model*

The [Makefile](COPSE-runtime/Makefile) also has a `sally-single` target which builds a benchmark that runs only the specified vectorized model. 

In addition to the build options listed above, the `sally-single` target accepts an additional option `model=` to specify the file containing the vectorized model to use for the benchmark.
An example invocation looks like:
```
make -j8 sally-single model=/path/to/vectorized.out THREADING=on
```

*Running benchmarks*
Building a benchmark produces a binary file with the same name as the build target used (for example, `sally-tiny` or `sally-single`).
This binary accepts two command line arguments: the number of queries to randomly generate, and the number of times to execute each query.

For example, the command to run the `sally-tiny` benchmark set by randomly generating 5 queries and executing them 3 times each is:
```
./sally-tiny 5 3
```

If no values are supplied, the default is generating 3 queries and executing each one once.
To replicate the experiments in the paper, use the values 9 and 5 (i.e. 9 queries, 5 times each).
However, we do not recommend doing this unless your computer is sufficiently powerful, as the full run takes a *lot* of time.

### Running the baseline implementation
All of the code for the baseline implementation can be found in the [Baseline](Baseline) directory.
We have provided a script called [baselineScript.py](Baseline/baselineScript.py) to automate running these.
The usage of this script is as follows:
```
python3 baselineScript.py (parallel|serial) benchmark-name num-threads num-iterations
```

The possible benchmark sets are:

| `benchmark-name` | Benchmarks run  |
|------------------|---------------- |
| all              | all twelve models |
| 1hr              | depth4, depth5, width55, prec8 |
| micro            | all except `income5`, `income15`, `soccer5`, `soccer15` |
| macro5           | `income5` and `soccer5` |
| macro15          | `income15` and `soccer15` |

Additionally, a particular model (such as `depth4` or `prec16`) can also be specified for `benchmark-name` to only run that model.

NOTE: If you are running the baseline script outside the docker container, make sure you export all the library and include paths to intel tbb and HElib in your current terminal.

Once the script is invoked, it will ask the user for input to specify some paths, these are:

1.) Enter the path to the current Directory: 

For this prompt enter the full path name to current directory. If you build using the docker image this will be /root

2.) Enter the full library path to intel tbb:

Enter the path name to lib directory for intel tbb, for the docker image this is /root/IntelTBB/lib/

3.) Enter the full include path to intel tbb:

Enter the full path name to the include directory for intel tbb, for the docker image this is /root/IntelTBB/include/

4.) Enter the full library path to intel HElib:

Enter the full path name to the lib directory for Helib, for the docker image this is /root/HElib/build/helib_pack/lib/

5.) Enter the full include path to intel HElib:

Enter the full path name to the include directory for Helib, for the docker image this is /root/HElib/build/helib_pack/include/

## Demo
This repository also comes with a Dockerfile that builds a container with all necessary dependencies installed.

* To build the container: `docker build --tag copse .`
* To spawn a shell in the container: `docker run -it copse bash`
