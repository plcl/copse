#Download base image ubuntu 18.04
FROM ubuntu:18.04
# Python 3 for running the scrips
FROM python:3
# set a directory for the the source code
WORKDIR /root
# copy all the files to the container
COPY . . 

# LABEL about the custom image
LABEL maintainer="PLCL"
LABEL version="0.1"
LABEL description="This is a Docker image for the COPSE paper"

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt-get update && apt-get -y upgrade

#install Vim to view .csv files
RUN apt-get update && apt-get install vim -y && apt-get install nmap -y

#install numpy
RUN pip install numpy openpyxl pandas sklearn python-csv python-math

#clone Helib version 1.1.0
RUN git clone --branch v1.1.0 https://github.com/homenc/HElib.git

#install dependencies and build Helib
RUN apt update && \
    apt install -y g++-7 nano && \
    apt install -y build-essential wget git cmake m4 libgmp-dev file && \
    cd ~ && \
    wget https://www.shoup.net/ntl/ntl-11.4.1.tar.gz && \
    tar --no-same-owner -xf ntl-11.4.1.tar.gz && \
    cd ntl-11.4.1/src && \
    ./configure SHARED=on NTL_GMP_LIP=on NTL_THREADS=on NTL_THREAD_BOOST=on && \
    make -j4 && \
    make install && \
    cd ~ && \
    mkdir HElib/build && \
    cd HElib/build && \
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DBUILD_SHARED=ON -DENABLE_THREADS=ON .. && \
    make -j4 && \
    make install && \
    ldconfig

#clone Intel Tbb and build it from source
RUN git clone https://github.com/wjakob/tbb.git
RUN cd tbb/build && cmake -DCMAKE_INSTALL_PREFIX=~/.local .. && make -j 4 && make install

#set the Environment variables
ENV PATH=/root/.local/bin:$PATH
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/root/.local/lib/:$LD_LIBRARY_PATH
ENV PATH=/root/.local/include/:$PATH
#ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/root/.local/lib/:$LD_LIBRARY_PATH

# install haskell stack
RUN curl -sSL https://get.haskellstack.org/ | sh

# build copse compiler
RUN cd COPSE-compiler && stack build && stack install
RUN cd COPSE-runtime && make -j4

CMD ["bash"]
