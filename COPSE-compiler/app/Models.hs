module Models where

import DecisionTree

data Regions
    = A
    | B
    | C
    deriving (Eq, Enum, Show, Read)

data Axes
    = X
    | Y
    deriving (Eq, Enum, Show, Read)


d4_1 =
    Branch
        (Threshold X 27)
        (Branch
             (Threshold X 184)
             (Branch
                  (Threshold X 225)
                  (Branch (Threshold X 96) (Label B) (Label B))
                  (Branch (Threshold X 243) (Label C) (Label B)))
             (Branch (Threshold X 172) (Label C) (Label A)))
        (Branch (Threshold Y 100) (Label A) (Label C))

d4_2 =
    Branch
        (Threshold Y 30)
        (Branch (Threshold Y 154) (Branch (Threshold X 121) (Label A) (Label A)) (Label C))
        (Branch
             (Threshold X 41)
             (Branch (Threshold Y 137) (Branch (Threshold X 152) (Label C) (Label A)) (Label A))
             (Branch (Threshold Y 88) (Label B) (Branch (Threshold Y 41) (Label A) (Label B))))

d4 = [d4_1, d4_2]

d5_1 =
    Branch
        (Threshold X 63)
        (Branch (Threshold X 238) (Label C) (Label B))
        (Branch
             (Threshold Y 245)
             (Label A)
             (Branch
                  (Threshold Y 217)
                  (Branch
                       (Threshold Y 108)
                       (Branch (Threshold X 182) (Label A) (Label C))
                       (Branch (Threshold Y 115) (Label A) (Label C)))
                  (Label A)))

d5_2 =
    Branch
        (Threshold X 231)
        (Branch
             (Threshold Y 181)
             (Branch
                  (Threshold Y 111)
                  (Label B)
                  (Branch
                       (Threshold Y 235)
                       (Branch (Threshold Y 77) (Label B) (Label C))
                       (Branch (Threshold X 24) (Label A) (Label A))))
             (Branch (Threshold Y 6) (Label B) (Label B)))
        (Branch (Threshold Y 169) (Label B) (Label B))

d5 = [d5_1, d5_2]

d6_1 =
    Branch
        (Threshold Y 19)
        (Branch
             (Threshold Y 59)
             (Branch
                  (Threshold Y 141)
                  (Branch
                       (Threshold Y 218)
                       (Branch
                            (Threshold X 3)
                            (Branch (Threshold X 42) (Label A) (Label A))
                            (Branch (Threshold Y 159) (Label B) (Label B)))
                       (Label A))
                  (Label C))
             (Label B))
        (Label B)

d6_2 =
    Branch
        (Threshold Y 214)
        (Branch
             (Threshold Y 241)
             (Branch
                  (Threshold Y 233)
                  (Branch
                       (Threshold X 165)
                       (Branch
                            (Threshold X 55)
                            (Branch (Threshold X 65) (Label C) (Label B))
                            (Branch (Threshold Y 247) (Label C) (Label A)))
                       (Label C))
                  (Branch (Threshold X 88) (Label B) (Label C)))
             (Label B))
        (Label A)

d6 = [d6_1, d6_2]

w10_1 =
    Branch
        (Threshold Y 156)
        (Label B)
        (Branch
             (Threshold X 251)
             (Label B)
             (Branch
                  (Threshold Y 124)
                  (Branch (Threshold X 113) (Branch (Threshold X 109) (Label C) (Label A)) (Label B))
                  (Label B)))

w10_2 =
    Branch
        (Threshold Y 85)
        (Branch
             (Threshold Y 161)
             (Label A)
             (Branch
                  (Threshold Y 197)
                  (Label A)
                  (Branch (Threshold Y 212) (Label A) (Branch (Threshold Y 99) (Label B) (Label C)))))
        (Label A)

w10 = [w10_1, w10_2]

w15_1 =
    Branch
        (Threshold X 205)
        (Branch (Threshold Y 178) (Label C) (Label C))
        (Branch
             (Threshold Y 242)
             (Branch
                  (Threshold Y 201)
                  (Label B)
                  (Branch (Threshold X 59) (Branch (Threshold X 185) (Label B) (Label B)) (Label B)))
             (Branch (Threshold X 127) (Label B) (Label A)))

w15_2 =
    Branch
        (Threshold Y 104)
        (Branch
             (Threshold Y 228)
             (Label A)
             (Branch
                  (Threshold X 217)
                  (Branch
                       (Threshold Y 172)
                       (Branch (Threshold X 26) (Label C) (Label C))
                       (Branch (Threshold X 101) (Label A) (Label C)))
                  (Branch (Threshold Y 88) (Label A) (Label B))))
        (Branch (Threshold Y 21) (Label B) (Label C))

w15 = [w15_1, w15_2]

w20_1 =
    Branch
        (Threshold Y 22)
        (Branch
             (Threshold Y 140)
             (Label B)
             (Branch
                  (Threshold Y 33)
                  (Branch (Threshold X 88) (Branch (Threshold X 149) (Label C) (Label A)) (Label C))
                  (Label A)))
        (Branch (Threshold X 182) (Label A) (Label C))

w20_2 =
    Branch
        (Threshold Y 46)
        (Branch
             (Threshold X 29)
             (Branch
                  (Threshold Y 57)
                  (Branch (Threshold X 46) (Branch (Threshold X 34) (Label B) (Label B)) (Label C))
                  (Branch (Threshold X 161) (Label A) (Branch (Threshold Y 65) (Label C) (Label C))))
             (Label B))
        (Label C)

w20_3 =
    Branch
        (Threshold X 224)
        (Branch
             (Threshold X 200)
             (Branch
                  (Threshold Y 247)
                  (Label B)
                  (Branch
                       (Threshold X 9)
                       (Branch (Threshold X 26) (Label B) (Label B))
                       (Branch (Threshold X 79) (Label C) (Label A))))
             (Label C))
        (Branch (Threshold Y 218) (Label B) (Label A))

w20 = [w20_1, w20_2, w20_3]

model =
    Branch
        (Threshold X 2)
        (Branch (Threshold Y 3) (Label A) (Branch (Threshold Y 5) (Label B) (Label C)))
        (Branch (Threshold X 4) (Label A) (Label B))
