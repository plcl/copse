{-# LANGUAGE FlexibleInstances #-}

module Main where

import DecisionTree
import GenerateCpp
import Matrices
import TreeLeveling

import Control.Arrow
import System.Environment
import System.IO
import System.FilePath

instance (Monad m) => Semigroup (Kleisli m a a) where
    (<>) = (>>>)

instance (Monad m) => Monoid (Kleisli m a a) where
    mempty = Kleisli return

newtype L =
    L Int
    deriving (Eq, Show, Read)

showL :: [String] -> L -> String
showL labels (L x) = labels !! x

showLs :: [String] -> [L] -> String
showLs labels ls = ls >>= showL labels

newtype F =
    F Int
    deriving (Eq, Show, Read)

instance Enum L where
    succ (L x) = L (x + 1)
    pred (L x) = L (x - 1)
    toEnum = L
    fromEnum (L x) = x

instance Enum F where
    succ (F x) = F (x + 1)
    pred (F x) = F (x - 1)
    toEnum = F
    fromEnum (F x) = x

interp :: (Enum f) => [Int] -> DecisionTree f l -> l
interp feats (Label c) = c
interp feats (Branch (Threshold f v) y n)
    | val < v = interp feats y
    | otherwise = interp feats n
  where
    val = feats !! fromEnum f

model1 =
    [ Branch
          (Threshold (F 0) 2)
          (Branch
               (Threshold (F 1) 3)
               (Label (L 0))
               (Branch (Threshold (F 1) 5) (Label (L 1)) (Label (L 2))))
          (Branch (Threshold (F 0) 4) (Label (L 0)) (Label (L 1)))
    ]

-- forest = "Branch { threshold = Threshold {feat = F 0, val = 2}, left = Branch { threshold = Threshold {feat = F 1, val = 3} , left = Label (L 0), right = Branch { threshold = Threshold {feat = F 1, val = 5} , left = Label (L 1), right = Label (L 2)}}, right = Branch {threshold = Threshold {feat = F 0, val = 4}, left = Label (L 0), right = Label (L 1)}}"


showTree :: (Enum f, Enum l) => DecisionTree f l -> String
showTree (Label l) = "Tree(" ++ (show $ fromEnum l) ++ ")"
showTree (Branch t l r) = "Tree(Thresh(" ++ (show . fromEnum . feat $ t) ++ ", " ++ (show $ val t) ++ "), " ++ (showTree l) ++ ", " ++ (showTree r) ++ ")"

buildModel :: (Eq f, Enum f) => [Vectorized -> Either String Vectorized] -> Forest f l ->  Either String Vectorized
buildModel steps forest = do
    vec <- return $ vectorize forest
    vec <- (runKleisli . mconcat $ Kleisli <$> steps) vec
    -- vec <- verifyVectorWidth 720 $ vec
    return vec

main :: IO ()
main = do
    (action:file:_) <- getArgs
    contents <- readFile file
    let (labelDef:trees) = lines contents
        forest = read <$> trees :: Forest F L
        labels = read labelDef :: [String]
        printLbl = putStrLn . showLs labels

    -- print $ getLevels forest    

    let showShape mat = putStrLn $ (show $ rows mat) ++ "," ++ (show $ cols mat)
    let showDebugInfo model = do
            showShape $ (levelMatrices model) !! 0
            showShape $ decisionToBranch model

    let optSteps = [verifyVectorWidth 1000, return] ++ (if action == "compileOpt" then [return . constantFold] else [])

    if action == "compile" || action == "compileOpt"
    -- then (either putStrLn showDebugInfo) $ buildModel optSteps forest
    then (either putStrLn putStrLn) $ generate (takeBaseName file) <$> buildModel optSteps forest
    else if action == "interp" then do
        line <- getLine
        printLbl $ forest >>= fringe
        let feats = read line :: [Int] in printLbl $ interp feats <$> forest
    else if action == "convert" then
        mapM_ putStrLn $ showTree <$> forest
    else putStrLn $ "Invalid: " ++ action
    

    -- let for = read forest :: DecisionTree F L
    -- print for


    -- if action == "compile"
    -- then putStrLn $ vectorize forest
    -- else if action == "interpret"

    
    
    -- line <- getLine
    -- let feats = read line :: [Int]
    -- print $ forest >>= fringe
    -- print $ interp feats <$> forest
