import Matrices
import GenerateCpp

makeShuffleMat :: (Eq a) => [a] -> [a] -> Matrix Bool
makeShuffleMat src dst = 
    let melems i j = (src !! j == dst !! i)
     in Matrix melems (length dst) (length src)


main = do
    let shufMat1 = makeShuffleMat "abcde" "dac"
    let shufMat2 = makeShuffleMat "dac" "ddaacc"
    putStrLn $ generateMatrix "shufMat1" $ fromEnum <$> shufMat1
    putStrLn $ generateMatrix "shufMat2" $ fromEnum <$> shufMat2

    let shufMat3 = shufMat1 `matMul` shufMat2
    putStrLn "done"

    