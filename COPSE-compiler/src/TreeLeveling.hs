module TreeLeveling
    ( LevelDescription(..)
    , getLevels
    ) where

import DecisionTree

data LevelDescription =
    Level
        { thresholds :: [Maybe Int]
        , mask :: [Bool]
        -- , numTrees :: Int
        }
    deriving (Show)

instance Semigroup LevelDescription where
    (Level t1 m1) <> (Level t2 m2) = Level (t1 ++ t2) (m1 ++ m2)

instance Monoid LevelDescription where
    mempty = Level [] []

getLevel' :: Int -> Int -> DecisionTree f l -> LevelDescription
getLevel' _ _ (Label _) = Level [Nothing] [True]
getLevel' d idx tree
    | depth tree > d =
        getLevel' d (idx + 1) (left tree) <> getLevel' d (idx + (width . left) tree) (right tree)
    | otherwise =
        let w = width tree
            lw = width $ left tree
            rw = width $ right tree
         in Level (replicate w (Just idx)) $ replicate lw False ++ replicate rw True

getLevels :: Forest f l -> [LevelDescription]
getLevels model =
    let maxDepth = foldl max 0 $ depth <$> model
     in map ($ model) $ getLevel <$> [1 .. maxDepth]
  where
    getLevel d forest = mconcat $ zipWith (getLevel' d) (offsets forest) forest
