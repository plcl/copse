{-# LANGUAGE LambdaCase #-}

module DecisionTree
    ( Threshold(..)
    , DecisionTree(..)
    , Forest
    , width
    , depth
    , fringe
    , offsets
    ) where

import Data.List

data Threshold f =
    Threshold
        { feat :: f
        , val :: Int
        }
    deriving (Eq, Show, Read)

data DecisionTree f l
    = Label l
    | Branch
          { threshold :: Threshold f
          , left :: DecisionTree f l
          , right :: DecisionTree f l
          }
    deriving (Eq, Show, Read)

type Forest f l = [DecisionTree f l]

width :: DecisionTree f l -> Int
width =
    \case
        Label _ -> 1
        Branch _ l r -> width l + width r

depth :: DecisionTree f l -> Int
depth =
    \case
        Label _ -> 0
        Branch _ l r -> 1 + max (depth l) (depth r)

fringe :: DecisionTree f l -> [l]
fringe =
    \case
        Label l -> [l]
        Branch _ l r -> fringe l ++ fringe r

offsets :: Forest f l -> [Int]
offsets = map (\p -> sum (width <$> p) - length p) . inits