{-# LANGUAGE LambdaCase #-}

module GenerateCpp
    ( generate
    , generateMatrix
    ) where

import Data.List
import Matrices

-- -- For a vector we're going to generate a c++ std::vector<long>
-- -- For a matrix we're going to create a c++ std::vector<std::vector<long>>
generateVectorContents :: (Show a) => Vector a -> String
generateVectorContents vec =
    let contents = intercalate ", " . toList . fmap show
     in "{" ++ contents vec ++ "}"

generateVector :: String -> Vector Int -> String
generateVector name vec = name ++ " = " ++ generateVectorContents vec ++ ";"

generateMatrixContents :: (Show a) => Matrix a -> String
generateMatrixContents mat =
    "{\n\t" ++
    intercalate ",\n\t" (generateVectorContents . fromRow mat <$> [0 .. rows mat - 1]) ++ "}"

generateMatrix :: (Show a) => String -> Matrix a -> String
generateMatrix name mat = name ++ " = " ++ generateMatrixContents mat ++ ";"

generate :: String -> Vectorized -> String
generate name vec =
    let tvec =
            generateVector ("ptxt_" ++ name ++ ".thresholds") $
            (\case
                 Nothing -> 0
                 Just c -> c) <$>
            allThresholds vec
        tmat = generateMatrix ("ptxt_" ++ name ++ ".d2b") $ fromEnum <$> decisionToBranch vec
        lmat = "ptxt_" ++ name ++ ".level_b2s = {\n\t" ++
            intercalate ",\n\n\t" (generateMatrixContents . fmap fromEnum <$> levelMatrices vec) ++
            "};"
        mvec = 
            "ptxt_" ++
            name ++
            ".level_mask = {\n\t" ++
            intercalate ",\n\t" (generateVectorContents . fmap fromEnum <$> levelMasks vec) ++ "};"
        kval = "ptxt_" ++ name ++ ".k = " ++ show (k vec) ++ ";"
        in "auto " ++ name ++ "()\n" ++
        "{\n\tPtxtModelDescription ptxt_" ++ name ++ ";\n\t" ++
        "ptxt_" ++ name ++ ".name = \"" ++ name ++ "\";\n\t" ++
        intercalate "\n\n\n\t" [tvec, mvec, lmat, tmat, kval] ++ "\n\n\treturn ptxt_" ++ name ++ ";\n}"
    --  in "PtxtModelDescription " ++ name ++ "()\n{\n" ++
    --  "\ttxtModelDescription ptxt_" ++ name ++ ";\n" ++
    --  "\t" ++ tvec ++ "\n" ++ 
    --  "\t" ++ tmat ++ "\n" ++ 
    --  "\t" ++ lmat ++ "\n" ++ 
    --  "\t" ++ mvec ++ "\n" ++ "ptxt_" ++ name ++ ".k = " ++ show (k vec) ++ ";"
