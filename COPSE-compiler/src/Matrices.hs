{-# LANGUAGE LambdaCase #-}

module Matrices
    ( thresholdVector
    , thresholdMatrix
    , levelMatrix
    , Matrix(..)
    , Vector(..)
    , toList
    , fromList
    , fromRow
    , Vectorized(..)
    , vectorize
    , branches
    , features
    , paddedVec
    , verifyVectorWidth
    , matMul
    , constantFold
    ) where

import Control.Monad
import Data.Bool
import Data.List
import DecisionTree
import TreeLeveling

-- type Matrix a = Int -> Int -> a
-- type Vector a = Int -> a
data Matrix a =
    Matrix
        { melems :: Int -> Int -> a
        , rows :: Int
        , cols :: Int
        }

data Vector a =
    Vector
        { velems :: Int -> a
        , size :: Int
        }

data BranchInfo f =
    BranchInfo
        { feature :: f
        , threshold :: Int
        , index :: Int
        }
    deriving (Show)

instance Functor Vector where
    fmap func vec = Vector (func . velems vec) (size vec)

instance Functor Matrix where
    fmap func mat = Matrix (\i j -> func (melems mat i j)) (rows mat) (cols mat)

instance (Show a) => Show (Vector a) where
    show vec = show $ velems vec <$> [0 .. size vec - 1]


transpose :: Matrix a -> Matrix a
transpose (Matrix elem r c) = (Matrix elem' c r) where elem' i j = elem j i

matMul :: (Num n) => Matrix n -> Matrix n -> Matrix n
matMul a b = let prodElem i j = sum $ (\k -> (melems a i k) * (melems b k j)) <$> [0..cols a - 1]
             in Matrix prodElem (rows a) (cols b)

instance Num Bool where
    x + y = not (x == y)
    (*) = (&&)
    abs = id
    negate = id
    signum _ = 1
    fromInteger = not . (==0)

fromList :: [a] -> Vector a
fromList lst = Vector (lst !!) (length lst)

fromRow :: Matrix a -> Int -> Vector a
fromRow mat row = Vector (melems mat row) (cols mat)

toList :: Vector a -> [a]
toList vec = velems vec <$> [0 .. size vec - 1]

branchesTree :: Int -> DecisionTree f l -> [BranchInfo f]
branchesTree offset =
    let branches' idx =
            \case
                Label c -> []
                Branch t l r ->
                    [BranchInfo (feat t) (val t) idx] ++
                    branches' (idx + 1) l ++ branches' (idx + width l) r
     in branches' offset

-- getOffset :: Forest f l -> Int
-- getOffset p = sum (width <$> p) - length p

-- offsets :: Forest f l -> [Int]
-- offsets forest = map getOffset (inits forest)

branches :: Forest f l -> [BranchInfo f]
branches forest = join $ zipWith branchesTree (offsets forest) forest

-- branches forest = forest >>= branchesTree
featureSortedBranches :: (Enum f) => Forest f l -> [BranchInfo f]
featureSortedBranches = sortOn (fromEnum . feature) . branches

features :: (Enum f, Eq f) => Forest f l -> [[BranchInfo f]]
features = groupBy (\x y -> Matrices.feature x == Matrices.feature y) . featureSortedBranches

-- maximum number of times a feature appears in a tree
maxK :: (Enum f, Eq f) => Forest f l -> Int
maxK = foldl max 0 . map length . group . map Matrices.feature . featureSortedBranches

-- pads out feature-sorted branch information with Nothing to max K
paddedVec :: (Enum f, Eq f) => Forest f l -> Vector (Maybe (BranchInfo f))
paddedVec forest =
    fromList (join . map (\row -> take k $ map return row ++ repeat Nothing) $ features forest)
  where
    k = maxK forest

-- vector of all threshold values (padded with Nothings for decision compares)
thresholdVector :: (Enum f, Eq f) => Forest f l -> Vector (Maybe Int)
thresholdVector = fmap (fmap Matrices.threshold) . paddedVec

thresholdMatrix :: (Enum f, Eq f) => Forest f l -> Matrix Bool
thresholdMatrix forest =
    Matrix
        (\j i ->
             case velems (paddedVec forest) i of
                 Nothing -> False
                 Just c -> Matrices.index c == j)
        (sum (width <$> forest) - length forest)
        (size $ paddedVec forest)

levelMatrix :: Int -> LevelDescription -> Matrix Bool
levelMatrix num ld =
    Matrix
        (\j i ->
             case thresholds ld !! j of
                 Nothing -> False
                 Just c -> c == i)
        (length $ thresholds ld)
        (length (thresholds ld) - num)

data Vectorized =
    Vectorized
        { allThresholds :: Vector (Maybe Int)
        , decisionToBranch :: Matrix Bool
        , levelMatrices :: [Matrix Bool]
        , levelMasks :: [Vector Bool]
        , k :: Int
        }

vectorize :: (Enum f, Eq f) => Forest f l -> Vectorized
vectorize forest = Vectorized tvec tmat lmats masks k
  where
    tvec = thresholdVector forest
    tmat = thresholdMatrix forest
    lmats = levelMatrix (length forest) <$> getLevels forest
    masks = fromList . mask <$> getLevels forest
    k = maxK forest


constantFold :: Vectorized -> Vectorized
constantFold (Vectorized tvec tmat lmats masks k) = 
    Vectorized tvec (Matrix undefined 0 0) ((`matMul` tmat) <$> lmats) masks k

verifyVectorWidth :: Int -> Vectorized -> Either String Vectorized
verifyVectorWidth w v@(Vectorized _ tmat _ _ _)
    | cols tmat > w = Left $ "Width of " ++ (show $ cols tmat) ++ " cannot fit into vector width " ++ show w
    | otherwise = Right v